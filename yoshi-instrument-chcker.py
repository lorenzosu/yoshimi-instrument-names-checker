""" Script to detect possible name clashes in Yoshimi instruments """
import os
import sys
import gzip
import argparse
import logging
import glob
import shutil
import xml.etree.ElementTree as ET

from dataclasses import dataclass

# global variables
# Lead digits in filenames, currently 4
LEAD_DIGITS = 4
LOGLEVEL = logging.INFO
INST_NAME_XPATH = "./INSTRUMENT/INFO/string[@name='name']"
XML_DECLARE_AND_DOC_TYPE = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE ZynAddSubFX-data>
"""


@dataclass
class InstrumentClass:
    """ Simple dataclass representing an 'instrument' """
    bank: str
    full_path: str
    name_from_file: str
    name_from_xml: str
    has_name_issue: bool
    xml: ET.ElementTree
    xml_name_element: ET.Element
    filename_only: str
    number_in_bank: str
    new_name: str = ""


def arguments_parse(parser):
    """ Parse command line arguments """
    parser.add_argument(
        "--verbose",
        help="increase output verbosity",
        action="store_true")
    parser.add_argument(
        "--ignore-underscores",
        help=(
            "Ignore underscores and treat them like spaces "
            "(e.g. 'my_intrument' == 'my instrument'"),
        action="store_true")
    parser.add_argument(
        "--bankroot",
        type=str,
        required=True,
        help="Input banks root path, e.g. /usr/local/share/yoshimi/banks")
    parser.add_argument(
        "--clean",
        action="store_true",
        help="clean names procedure. Requires the --outputroot parameter")
    parser.add_argument(
        "--report",
        action="store_true",
        help="print a report of possible issues")
    parser.add_argument(
        "--outputroot",
        help=(
            "root directory for the celeaned bank if --clean is specified. "
            "The root directory will be created if needed")
        )

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(
                format='%(levelname)s: %(message)s',
                level=logging.DEBUG
                )
    else:
        logging.basicConfig(
                format='%(levelname)s: %(message)s',
                level=LOGLEVEL
                )

    ignore_unersc = bool(args.ignore_underscores)

    bank_root_dir = args.bankroot
    check_exist_dir_or_exit(
            bank_root_dir,
            "Bank root provided with --bankroot"
            )

    print_rep = bool(args.report)

    if args.clean and (args.outputroot is None):
        parser.error("--clean requires --outputroot")
    elif args.clean:
        output_root = args.outputroot
        root_parent = os.path.split(output_root)[0]
        check_exist_dir_or_exit(
            root_parent,
            f"Output root top dir provided with --outputroot ({output_root})")
        clean_action = True
    else:
        clean_action = False
        output_root = None

    return (clean_action, bank_root_dir, output_root, ignore_unersc, print_rep)


def check_exist_dir_or_exit(dir_path, message_string=""):
    """ Check if a directory exists, otherwise exit with an error message """
    error_message = f'Provided directory dose not exist "{dir_path}" - Exiting'
    if message_string != "":
        error_message = message_string + " - " + error_message
    if not os.path.exists(dir_path):
        logging.error(error_message)
        sys.exit(1)


def file_process(filename):
    """Extract the instrument name from (just) the filename. And the number

    Example: "0001-Arpeggio1.xiz" should return ("Arpeggio1", "0001-")
    """
    basename = os.path.splitext(filename)[0]
    name = basename[LEAD_DIGITS + 1:]
    number_string = basename[:LEAD_DIGITS + 1]
    return name, number_string


def find_string_name(xml_root):
    """Find the name string in the instrument XML

    xml_root must be the root element as ElementTree
    """
    name_element = xml_root.find(INST_NAME_XPATH)
    name = name_element.text
    if name is None:
        name = ""
    return name, name_element


def get_xml_root(full_file_path):
    """ Get the XML root as ElementTree from instrument full_file_path """
    try:
        with gzip.open(full_file_path, 'r') as gz_file:
            file_string = gz_file.read()
    except gzip.BadGzipFile as err:
        logging.debug("-- Unable to decompress gzip: %s", err)
        logging.debug("-- Trying to see if it's a plain-text xml file...")
        with open(full_file_path, 'r') as text_file:
            file_string = text_file.read()

    try:
        root = ET.fromstring(file_string)
    except ET.ParseError as err:
        logging.debug("-- Element Tree Parser error: %s", err)
        logging.debug("-- Trying to strip file")
        file_string = file_string.strip()
        root = ET.fromstring(file_string)
    return root


def detect_issues(bank_directory, ignore_underscores):
    """ Detect possible name clashes between filename and XML string """
    instrument_dirs = sorted(
        glob.glob(os.path.join(bank_directory, "*"))
    )
    output_list = []
    for dir_current in instrument_dirs:
        file_list = sorted(glob.glob(os.path.join(dir_current, "*.xiz")))
        bank_name = os.path.split(dir_current)[1]
        logging.info("---- Processing bank: %s ...", bank_name)
        for file_current in file_list:
            logging.debug("- Processing file: %s", file_current)
            name_from_file, number_part = file_process(
                    os.path.split(file_current)[1]
                    )
            filename_only = os.path.split(file_current[1])

            root_current = get_xml_root(file_current)
            name_from_xml, mame_element = find_string_name(root_current)
            logging.debug(" -- Name detected from file: %s", name_from_file)
            logging.debug(" -- Name detected from XML:  %s", name_from_xml)

            original_names_differ = name_from_file != name_from_xml
            skip_due_to_underscore = (
                ignore_underscores and
                (name_from_file == name_from_xml.replace(" ", "_"))
            )

            logging.debug("Creating Instrument Class")
            instrument_current = InstrumentClass(
                    bank = bank_name,
                    full_path = file_current,
                    name_from_file = name_from_file,
                    name_from_xml = name_from_xml,
                    has_name_issue = False,
                    xml = root_current,
                    xml_name_element = mame_element,
                    filename_only = filename_only,
                    number_in_bank = number_part,
                    new_name = ""
                    )

            if original_names_differ and not skip_due_to_underscore:
                instrument_current.has_name_issue = True
                logging.debug(" -- ADDING ISSUE for this")
            else:
                if skip_due_to_underscore:
                    logging.debug(
                        "-- Original names differed,"
                        "but ignoring because they use underscores"
                    )
                logging.debug(" -- NO ISSUE added")
            output_list.append(instrument_current)

    return output_list


def print_report(list_of_instruments, ignore_underscores):
    """ Print a report of the possible issues """
    num_of_issues = len([x for x in list_of_instruments if x.has_name_issue])
    print(
            f"-- Found a total of {num_of_issues} "
            "possible problematic files --"
        )
    if ignore_underscores:
        print(
                "Underscores where ignored"
                " (e.g. 'my_instrument' treated as equal to 'my instrument')"
        )
    last_bank = ""
    for iss in list_of_instruments:
        if not iss.has_name_issue:
            continue
        if iss.name_from_file == "":
            xml_explanation = " [empty]"
        else:
            xml_explanation = ""
        if iss.bank != last_bank:
            print(f"\n# Bank: {iss.bank}")
            last_bank = iss.bank

        print(f" - File {iss.full_path}")
        print(f"  -- Name from filename:  '{iss.name_from_file}'")
        print(
                f"  -- Name from XML:       '{iss.name_from_xml}'" +
                f"{xml_explanation}"
            )

def get_numeric_input(choices, message):
    """ Get a numeric input from the user. Repeat until a valid input is given """
    user_input = None
    while user_input not in choices:
        try:
            user_input = int(input(message))
        except ValueError:
            pass
    return user_input


def get_yes_no_input(default=None, prompt=""):
    """Get a yes/no input from user.
    Accepted: "y", "Y", "n", "N", "yes", "no"

    optional default can be None (no default, 'y' or 'n' """
    yes_inputs = {"y", "Y", "yes", "YES"}
    no_inputs = {"n", "N", "no", "NO"}
    if default == 'y':
        yes_inputs.add("")
        default_string = "[Y]? "
    elif default == 'n':
        no_inputs.add("")
        default_string = "[N]? "
    else:
        default_string = "? "
    message = (f"{prompt}Yes or no (y/n) {default_string}")
    user_input = None
    while user_input not in yes_inputs.union(no_inputs):
        user_input = input(message)

    return user_input in yes_inputs


def create_dir_with_prompt(root_path):
    """ Prompt user to create a dir, if they say yes do it. """
    print(f"Directory {root_path} does not exist, create it?")
    create = get_yes_no_input()
    if not create:
        logging.debug("User did not want to creat dir %s", root_path)
        return False
    return create_dir(root_path)


def create_dir(dir_path):
    """ Create a directory with some exception handling """
    try:
        os.makedirs(dir_path)
    except OSError as err:
        logging.debug("Could not create dir %s - Error is %s", dir_path, err)
        return False
    return os.path.exists(dir_path)


def generate_new_bank_names(instr_list):
    """ Update the instrument list with new names, interactively """
    last_bank = ""
    go_on = True

    # first fix names, then create or copy the files
    for iss in instr_list:
        if not iss.has_name_issue:
            logging.debug(
                "Skipping: no name issue had been detected: %s",
                iss.full_path)
            continue

        if not go_on:
            iss.has_name_issue = False
            logging.debug("Skipping due to rename-abort: %s", iss.full_path)
            continue

        if iss.bank != last_bank:
            print(f"\n# Bank: {iss.bank}")
            last_bank = iss.bank

        print(f"- File {iss.full_path}")
        user_options = {
            1: (f"Use name from XML      ['{iss.name_from_xml}']"),
            2: (f"Use name from filename ['{iss.name_from_file}']"),
            3: ("Rename manually"),
            4: ("Leave as is."),
            5: ("Abort renaming (all files will still be generated)")
        }
        prompt_message = "\n".join(
            [str(key) + ": " + user_options[key] for key in user_options]
        )
        prompt_message += "\n"
        choice = get_numeric_input(user_options.keys(), prompt_message)
        if choice == 1:
            iss.new_name = iss.name_from_xml
        if choice == 2:
            iss.new_name = iss.name_from_file
            make_new_xml(iss)
        if choice == 3:
            confirm = False
            while not confirm:
                new_n = input("Enter new name: ")
                print(f"New name will be '{new_n}'. Confirm:")
                confirm = get_yes_no_input(default='y')
            iss.new_name = new_n
            make_new_xml(iss)
        if choice == 4:
            iss.has_name_issue = False
        elif choice == 5:
            iss.has_name_issue = False
            go_on = False


def make_new_isntr_filename(instr):
    """ Generate a new filename for the instrument """
    new_filename = instr.number_in_bank + instr.new_name + ".xiz"
    return new_filename


def make_new_xml(instr):
    """ Generate a new xml for the instrument """
    new_root = get_xml_root(instr.full_path)
    new_root.find(INST_NAME_XPATH).text = instr.new_name
    instr.xml = new_root

def make_new_instruments(instr_list_new, root_instr_new_path):
    """ After running generate_new_bank_names, actually create the new bank """
    if not os.path.exists(output_root_dir):
        create_ok = create_dir_with_prompt(root_instr_new_path)
        if not create_ok:
            logging.debug(
                "Output root dir does not exist or could not create %s",
                root_instr_new_path
                )
            return False
    logging.info("Copying files to new directory %s", root_instr_new_path)
    bank_current = ""
    for iss in instr_list_new:
        if iss.bank != bank_current:
            bank_current = iss.bank
            dir_curr_bank = os.path.join(root_instr_new_path, bank_current)
            create_dir(dir_curr_bank)
            logging.debug("- Creating dir: %s", dir_curr_bank)
        if iss.has_name_issue:
            xml_string = XML_DECLARE_AND_DOC_TYPE.encode('utf-8')
            xml_string += ET.tostring(iss.xml, encoding='utf-8')
            dest_file = make_new_isntr_filename(iss)
            dest_full_path = os.path.join(dir_curr_bank, dest_file)
            logging.debug("-- Creating new file %s -> %s", iss.full_path, dest_full_path)
            with gzip.open(dest_full_path, 'wb') as fgz:
                fgz.write(xml_string)
        else:
            dest_file = os.path.split(iss.full_path)[1]
            dest_full_path = os.path.join(dir_curr_bank, dest_file)
            logging.debug("-- Copying file %s -> %s", iss.full_path, dest_full_path)
            shutil.copyfile(iss.full_path, dest_full_path)
    return True

if __name__ == "__main__":
    main_parser = argparse.ArgumentParser()
    do_clean, bank_dir, output_root_dir, ignr_under, do_rep = arguments_parse(
        main_parser
        )
    all_instruments = detect_issues(bank_dir, ignr_under)
    if do_rep:
        print_report(all_instruments, ignr_under)
    if do_clean:
        generate_new_bank_names(all_instruments)
        make_new_instruments(all_instruments, output_root_dir)
